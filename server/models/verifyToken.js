module.exports = (sequelize, DataTypes) => {
    const VerificationToken = sequelize.define('verificationToken', {
        userId: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        token: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        }
    });
    return VerificationToken;
  };
  
