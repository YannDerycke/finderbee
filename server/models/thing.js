module.exports = (sequelize, DataTypes) => {
    const Thing = sequelize.define('thing', {
      name: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },
      description: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },
      picture: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },
      picturePublicId: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },
      geo: {
        type: DataTypes.GEOGRAPHY('POINT', 4326),
        unique: false,
        allowNull: false
      },
      radius: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: true
      },
      status: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },
      categoryId: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: false
      },
      typeId: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: false
      },
      reward: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: true
      },
      userId: {
        type: DataTypes.INTEGER,
        unique: false,
        allowNull: false
      },
      date: {
        type: DataTypes.DATE,
        unique: false,
        allowNull: false
      },
      verified: {
        type: DataTypes.BOOLEAN,
        unique: false,
        allowNull: false
      },
    });
    Thing.associate = function(models) {
      Thing.belongsTo(models.Category, {foreignKey: 'categoryId', as: 'category', allowNull: false});
      Thing.belongsTo(models.Type, {foreignKey: 'typeId', as: 'type', allowNull: false});
      Thing.belongsTo(models.User, {foreignKey: 'userId', as: 'user', allowNull: false});
      Thing.hasMany(models.Notification, {
        foreignKey: 'thingId',
        onDelete: 'CASCADE'
      });
    };
    return Thing;
  };