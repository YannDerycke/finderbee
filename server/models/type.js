module.exports = (sequelize, DataTypes) => {
    const Type = sequelize.define('type', {
      name: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
      }
    });
    return Type;
  };
  
