module.exports = (sequelize, DataTypes) => {
    const Notification = sequelize.define('notification', {
        userId: {
            type: DataTypes.INTEGER,
            unique: false,
            allowNull: false
          },
        thingId: {
            type: DataTypes.INTEGER,
            unique: false,
            allowNull: false
        },
        type: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: false
          },
        status: { 
            type: DataTypes.STRING,
            unique: false,
            allowNull: false
        },
    });
    Notification.associate = function(models) {
        models.Thing.belongsTo(models.Category, {foreignKey: 'categoryId', as: 'categoryt', allowNull: false});
        models.Thing.belongsTo(models.Type, {foreignKey: 'typeId', as: 'typet', allowNull: false});
        Notification.belongsTo(models.Thing, {foreignKey: 'thingId', as: 'thing', allowNull: false, onDelete: 'cascade',
        hooks: true})
        Notification.belongsTo(models.User, {foreignKey: 'userId', as: 'user', allowNull: false})
        
    };
    return Notification;
  };
  