module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('user', {
      name: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },
      surname: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
      },
      language: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },
      password: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: true
      },
      profile: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },
      verified: {
        type: DataTypes.BOOLEAN,
        unique: false,
        allowNull: false
      }, 
      pushnotification: {
        type: DataTypes.STRING,
        unique: false,
        allowNull: false
      },
    });

    User.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
        delete values.password;
        return values;
      }

    // User.findByLogin = async login => {
    //     let user = await User.findOne({
    //       where: { name: login },
    //     });
    
    //     if (!user) {
    //       user = await User.findOne({
    //         where: { email: login },
    //       });
    //     }
    
    //     return user;
    //   };
    User.associate = function(models) {
      User.hasMany(models.Notification, {
        foreignKey: 'userId',
        onDelete: 'CASCADE'
      });
    };
    return User;
  };
  
