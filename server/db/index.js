const Sequelize = require('sequelize');

const sequelize = new Sequelize(`postgres://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}:${process.env.DB_PORT}/danmcke41bqjup?ssl=true`);

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

  const models = {
    User: sequelize.import('../models/user'),
    Category: sequelize.import('../models/category'),
    Type: sequelize.import('../models/type'),
    Thing: sequelize.import('../models/thing'),
    VerificationToken: sequelize.import('../models/verifyToken'),
    Notification: sequelize.import('../models/notification'),
  };

  Object.keys(models).forEach(key => {
    if ('associate' in models[key]) {
      models[key].associate(models);
    }
  });

// const Pool = require("pg").Pool;
// const pool = new Pool({
//   user: process.env.DB_USER,
//   host: process.env.DB_HOST,
//   database: "danmcke41bqjup",
//   password: process.env.DB_PASS,
//   port: process.env.DB_PORT,
//   ssl: true
// });

module.exports = {models, sequelize};
