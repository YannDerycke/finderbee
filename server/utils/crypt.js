const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {
    hash:  (toHash) => {
      return bcrypt.hash(toHash, saltRounds)
    },
    compare: (passwordToTest, password) => {
      return bcrypt.compare(passwordToTest,password)
    }
}