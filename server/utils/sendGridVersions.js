module.exports = {
    lostAdded: {
        fr: "d-cd41cc1546fc4007bfb4b5e916c06b0d",
        es: "d-5dcf54b324fa4045b3c6cf24bf51b67e",
        en: "d-2540029ca21949edaca75d77cc2e0393"

    },
    foundAdded: {
        fr: "d-948e60c4a5d94d3e83a3e5f4774f319f",
        es: "d-cb4af46370744724948e8a1091b086a3",
        en: "d-5125f49c5e32435480f953215c3e597c"
    },
    resetPassword: {
        fr: "d-552a8b0afa7b422f92cd7efc1bef6a3e",
        es: "d-2341a11827e8461f800b066a933d48c9",
        en: "d-75553597e7d54c298dbe35d20af1f6d9"
    },
    verifyEmail: {
        fr: "d-72270a7fea6d49c58d6efa07d15b5816",
        es: "d-11ddba31692b45abb1e9611ddd35f8e7",
        en: "d-e6cb2bcd2f3a43578dded876f40e0fe8"
    }
}