const jwt = require("jsonwebtoken");
const { JWT_SECRET } = require("../configuration");

module.exports = {
  ensureToken: (req, res, next) => {
    // req.decoded = {sub:1} //DELETE
    // next() //DELETE
    var bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== "undefined") {
      console.log("bearerHeaderIN", bearerHeader)
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
      console.log("bearerToken", bearerToken)
      jwt.verify(bearerToken, JWT_SECRET, (err, decoded) => {
        if (err) {
          res.status(403).send({message:'noAccess'});
        } else {
          req.token = bearerToken;
          req.decoded = decoded;
          console.log('reqq', req.decoded)
          next();
        }
      });
    } else {
      res.status(403).send({message:'noAccess'});
    }
  },
  signToken: user => {
    console.log('userTOsignin', user)
    return jwt.sign(
      {
        iss: "finderbee",
        sub: user.id,
        profile: user.profile,
        // email: user.email,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate() + 1)
      },
      JWT_SECRET
    );
  },
  checkToken: token => {
    console.log('toekn', token)
      return  jwt.verify(token, JWT_SECRET, (err, decoded) => {
          return decoded
      });
  }
};
