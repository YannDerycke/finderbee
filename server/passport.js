
const JwtStrategy = require("passport-jwt").Strategy;
const { ExtractJwt } = require("passport-jwt");
const LocalStrategy = require("passport-local").Strategy;
const FacebookTokenStrategy = require("passport-facebook-token");
const config = require("./configuration");
const User = require("./models/user");
const { JWT_SECRET } = require("./configuration");
const bcrypt = require('./utils/crypt');
// const jwtService = require("../utils/jswToken");
// const User = require("../models/user");
const db = require("./db");

var ex = function (passport, app) {
// JSON WEB TOKENS STRATEGY
  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });
  passport.use(
    new JwtStrategy(
      {
        jwtFromRequest: ExtractJwt.fromHeader("authorization"),
        secretOrKey: JWT_SECRET,
        passReqToCallback: true
      },
      async (req, payload, done) => {
        console.log('payload', payload)
        try {
          // Find the user specified in token
          const user = await User.findById(payload.sub);

          // If user doesn't exists, handle it
          if (!user) {
            return done(null, false);
          }

          // Otherwise, return the user
          req.user = user;
          done(null, user);
        } catch (error) {
          done(error, false);
        }
      }
    )
  );


  passport.use(
    "facebookToken",
    new FacebookTokenStrategy(
      {
        clientID: config.oauth.facebook.clientID,
        clientSecret: config.oauth.facebook.clientSecret,
        passReqToCallback: true
      },
      async (req, accessToken, refreshToken, profile, done) => {
        try {
          console.log("profile", profile);
          console.log("accessToken", accessToken);
          console.log("refreshToken", refreshToken);

          if (req.user) {
            // We're already logged in, time for linking account!
            // Add Facebook's data to an existing account
            req.user.methods.push("facebook");
            req.user.facebook = {
              id: profile.id,
              email: profile.emails[0].value
            };
            await req.user.save();
            return done(null, req.user);
          } else {
            // We're in the account creation process
            let existingUser = await User.findOne({ "facebook.id": profile.id });
            if (existingUser) {
              return done(null, existingUser);
            }

            // Check if we have someone with the same email
            existingUser = await User.findOne({
              "local.email": profile.emails[0].value
            });
            if (existingUser) {
              // We want to merge facebook's data with local auth
              existingUser.methods.push("facebook");
              existingUser.facebook = {
                id: profile.id,
                email: profile.emails[0].value
              };
              await existingUser.save();
              return done(null, existingUser);
            }

            const newUser = new User({
              methods: ["facebook"],
              facebook: {
                id: profile.id,
                email: profile.emails[0].value
              }
            });

            await newUser.save();
            done(null, newUser);
          }
        } catch (error) {
          done(error, false, error.message);
        }
      }
    )
  );

  // LOCAL STRATEGY
  passport.use(
    new LocalStrategy(
      {
        usernameField: "email"
      },
      async (email, password, done) => {

        try {
          db.models.User.findOne(
            {
              where: {
                email: email
              }
            }
          ).then(user => {
            if (user) {
              console.log('user', user)
              bcrypt.compare( password, user.password).then(function(check) {
                if (check) {
                  done(null, user);
                  // const token = jwtService.signToken(user);
                  res.status(200).send({
                    user: user,
                    ok: true,
                    message: "successSignIn",
                    token
                  });
                } else {
                  return done(null, false);
                }
              });
            } else {
              return done(null, false);
              //res.status(409).send({message:"errorSignIn"});
            }
        
          });
        }
        catch (error) {
          done(error, false);
        }
        // try {
        //   // Find the user given the email
        //   const user = await User.findOne({ "local.email": email });

        //   // If not, handle it
        //   if (!user) {
        //     return done(null, false);
        //   }

        //   // Check if the password is correct
        //   const isMatch = await user.isValidPassword(password);

        //   // If not, handle it
        //   if (!isMatch) {
        //     return done(null, false);
        //   }

        //   // Otherwise, return the user
        //   done(null, user);
        // } catch (error) {
        //   done(error, false);
        // }
      }
    )
  );
    return passport
  }
module.exports = ex;
