const express = require("express");
const fs = require("fs");
const passport = require("passport");
// // const passportService = require('./passport')(passport)
// console.log('passportS', passportService)

require("dotenv").config();
// const morgan = require("morgan");
const bodyParser = require("body-parser");
// const cors = require("cors");

const app = express();
// app.use(cors());

// Middlewares moved morgan into if for clear tests
// if (!process.env.NODE_ENV === "test") {
//   app.use(morgan("dev"));
// }
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(passport.initialize());


// Routes
app.use("/users", require("./routes/user"));
app.use("/pictures", require("./routes/pictures"));
app.use("/things", require("./routes/things"));
app.use("/categories", require("./routes/categories"));
app.use("/notifications", require("./routes/notifications"));
app.use("/types", require("./routes/types"));
app.use("/", function(req, res, next){
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<form action="things/upload" method="post" enctype="multipart/form-data">');
    res.write('<input type="file" name="filetoupload"><br>');
    res.write('<input type="text" name="description"><br>');
    res.write('<input type="text" name="category"><br>');
    res.write('<input type="submit">');
    res.write('</form>');
    return res.end();
})

// app.use(passportService.initialize());
// app.use(passportService.session());


module.exports = app;
