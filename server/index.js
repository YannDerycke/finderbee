const app = require("./app");
const db = require('./db');

const passport = require("passport");



// Start the server
const port = process.env.PORT || 5000;
db.sequelize.sync().then((x) => {
  app.listen(port, () => {
    
    console.log(`Example app listening on port ${process.env.PORT}!`)
    // app.use(passport.initialize());
    // app.use(passport.session());
  });
}, (e)=>{
  console.log('e', e)
}).catch((error)=>{
  console.log('errorSeq', error)
});
var os = require("os");
var ifaces = os.networkInterfaces();

Object.keys(ifaces).forEach(function(ifname) {
  var alias = 0;

  ifaces[ifname].forEach(function(iface) {
    if ("IPv4" !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }

    if (alias >= 1) {
      // this single interface has multiple ipv4 addresses
      console.log(ifname + ":" + alias, iface.address);
    } else {
      // this interface has only one ipv4 adress
      console.log(ifname, iface.address);
    }
    ++alias;
  });
});
