const jwt = require("jsonwebtoken");
// const User = require("../models/user");
const { JWT_SECRET } = require("../configuration");
const pool = require("../db");
const cloudinary = require("cloudinary");
cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
});

module.exports = {
  savePicture: (temp_path, file_name, userId) => {
    return new Promise((resolve, reject)=> {
      cloudinary.uploader.upload(
        temp_path,
        function(result) {
          if (!result.error) {
            resolve(result);
          } else {
            reject(result.error);
          }
        },
        {
          public_id: file_name,
          folder: "finderBee/" + userId,
          chunk_size: 6000000,
          eager: [
            { width: 50, height: 50, crop: "pad", audio_codec: "none" },
            {
              width: 160,
              height: 100,
              crop: "crop",
              gravity: "south",
              audio_codec: "none"
            }
          ],
          eager_async: true,
          eager_notification_url: "https://mysite.example.com/notify_endpoint"
        }
      );
    });
  }
};
