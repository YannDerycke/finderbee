const jwt = require("jsonwebtoken");
// const User = require("../models/user");
const { JWT_SECRET } = require("../configuration");
const db = require("../db");

module.exports = {
  save: (req, res, next) => {
    db.models.Notification.create(req.body
      ).then(user => {
        res.status(201).json(user);
      }).catch(error => {
        console.log('error', error)
        res.status(422).json(error);
      })
  },
  getMine: (req, res, next) => {
    db.models.Notification.findAll(
        {
            where: {
                userId: req.decoded.sub
            },
            include: [ {
              model: db.models.Thing,
              as: 'thing',
              include: ['type', 'category']
            }, {
              model: db.models.User,
              as: 'user',
            }]
        }
    ).then(users => {
      res.status(200).json(users);
    }).catch(error => {
      console.log('error', error)
      res.status(400).json(error);
    });
  },
  update: (req, res, next) => {
    
  },
  erase: (req, res, next) => {
    //console.log('thing eraseq in', req.params)
    db.models.Notification.destroy({
      where: {
        id:parseInt(req.params.id),
        userId: req.decoded.sub
      },
      raw: true,
    }
    ).then((thing) =>{
      console.log('notif deleted', thing)
      res.status(201).json(thing)
    })
  }
};
