const Sequelize = require("sequelize");
const db = require("../db");
const sgMail = require("@sendgrid/mail");
const pushNotification =  require('./pushNotification');
const { Expo } = require("expo-server-sdk");
const sendGridVersion = require ('../utils/sendGridVersions');
module.exports = {
    findFound: (thingToCheck, user) => {
        let typeId = 2;  //FOUND
        let queryGeo = Sequelize.fn('ST_DWithin',
        Sequelize.col('geo'),
        Sequelize.fn('ST_MakePoint', parseFloat(thingToCheck.geo.coordinates[0]), parseFloat(thingToCheck.geo.coordinates[1])),
        parseFloat(thingToCheck.radius)
    ) 
        let where = {
            $and:  Sequelize.where(queryGeo, true),
            verified: false, //TO CHANGE
            typeId, 
            // userId: { [Sequelize.Op.not]: user.sub}, //TO CHANGE
            categoryId: thingToCheck.categoryId
        }
        return db.models.Thing.findAll({
            where:where,
            include: [ 'category', 'user', 'type']}     
        )
    },
    findLost: (thingToCheck, user) => {
        let typeId = 3 // LOST
        let queryGeo = Sequelize.fn('ST_DWithin',
            Sequelize.col('geo'),
            Sequelize.fn('ST_MakePoint', parseFloat(thingToCheck.geo.coordinates[0]), parseFloat(thingToCheck.geo.coordinates[1])),
            Sequelize.col('radius')
        )
        let where = {
            $and:  Sequelize.where(queryGeo, true),
            verified: false, //TO CHANGE
            typeId, 
            // userId: { [Sequelize.Op.not]: user.sub}, //TO CHANGE
            categoryId: thingToCheck.categoryId
        }
        return db.models.Thing.findAll({
            where:where,
            include: [ 'category', 'user', 'type']}     
        ).then((thingsFound)=> {
            sgMail.setApiKey(process.env.SENDGRID_API_KEY);
            thingsFound.forEach(thing => {
                const msg = {
                to: thing.user.email,
                from: "info@finderbee.com",
                templateId: sendGridVersion.foundAdded[thing.user.language],
                //templateId: true ? "d-948e60c4a5d94d3e83a3e5f4774f319f":"", //FINISH LOCAL Thing Found Added FR
                dynamic_template_data: {
                    subject: "FOUND",
                    thing: thingToCheck,
                    name: `${thing.user.name} ${thing.user.surname}`,
                    Sender_adress: "17, rue du Canada 1190 Forest"
                }
                };
                sgMail.send(msg);
            })
        })
    },

}