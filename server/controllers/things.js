const jwt = require("jsonwebtoken");
// const User = require("../models/user");
const { JWT_SECRET } = require("../configuration");
const db = require("../db");
const Sequelize = require("sequelize");
const moment = require('moment');
const findThingFit = require('./findThingFit');

const cloudinary = require("cloudinary");
const formidable = require("formidable");
cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
});
const pictureService = require("./pictures")

module.exports = {
  erase: (req, res, next) => {
    db.models.Thing.findAll({
      where: {
        id:parseInt(req.params.id)
      },
      raw: true,
    }
    ).then((thing) =>{
      if (thing.length > 0) {
        db.models.Thing.destroy(
          {
            where: {id: thing[0].id},
            returning: true, // needed for affectedRows to be populated
            plain: true // makes sure that the returned instances are just plain objects
          }
          ).then(response => {
            cloudinary.uploader.destroy(thing[0].picturePublicId, function(result) { 
              res.status(200).send({message:'successDelete', response});
            });
            
          }).catch(error => {
            res.status(422).send({message:'error', error});
          })
      } else {
        res.status(422).send({message:'notAllowed'});
      }
    })
    .catch(next)
  },
  update: (req, res, next) => {
    req.body.userId = req.decoded.sub;
    // [ 4.351996,50.846609]

    try {
      const point = { type: 'Point', coordinates: [req.body.geo.long, req.body.geo.lat]};
      req.body.geo = point;
      req.body.status = "LOST";
    } catch(error) {
      res.status(400).json("coordonates missing");
    }

    db.models.Thing.update(req.body,
      {
        where: {
          id: req.params.id, 
          userId: req.decode.sub
        },
        returning: true, // needed for affectedRows to be populated
        plain: false // makes sure that the returned instances are just plain objects
      }
      ).then(user => {
        res.status(201).json(user);
      }).catch(error => {
        res.status(422).json(error);
      })
  },
  save: (req, res, next) => {
      var form = new formidable.IncomingForm();
      let currentFields;
      form.parse(req, function(error, fields, files) {
        if (error) {
          res.status(422).json(error);
        }
        currentFields = fields;
      })
      try {
        form.on("end", function(fields, files) {
          let temp_path = this.openedFiles[0].path;
          let file_name = this.openedFiles[0].name.split('.').slice(0, -1).join('.');
          let userId = parseInt(req.decoded.sub);
          let typeId = parseInt(currentFields.typeId);
          let categoryId = parseInt(currentFields.categoryId);
          let radius = parseInt(currentFields.radius) || 0;
          let reward = parseInt(currentFields.reward) || 0;
          pictureService.savePicture(temp_path, file_name, userId).then((result)=> {
            if (result) {
              const geo = JSON.parse(currentFields.geo);
              const payload = {
                userId,
                categoryId,
                typeId,
                name: currentFields.name,
                description : currentFields.description,
                geo : { type: 'Point', coordinates: [geo.coordinates[0], geo.coordinates[1]]},
                radius,
                reward,
                picture: result.url,
                picturePublicId: result.public_id,
                status: "LOST",
                date: new Date(currentFields.date).toISOString(),
                verified: false //TO CHANGE

              }
              db.models.Thing.create(payload,
                ).then(thing => {
                  thing.typeId === 2 ? findThingFit.findLost(thing.dataValues, req.decoded.sub) : findThingFit.findFound(thing.dataValues, req.decoded.sub)
                  res.status(201).send({message:'successCreate', thing});
                }).catch(error => {
                  cloudinary.uploader.destroy(result.public_id, function(result) { 
                    res.status(400).send({message:'errorCreate', error});
                  });
                })
            }
          }) 
        });
      } catch (error) {
        res.status(400).send({message:'errorCreate', error});
      }
  },
  get: (req, res, next) => {
    console.log('req.param.id', req.params.id)
    db.models.Thing.findOne({
      where: {id: req.params.id},
      include: [ 'category', 'user', 'type']
    }).then(thing => {
      console.log('thing', thing)
      res.status(200).json(thing);
    }).catch(error => {
      console.log('error', error)
      res.status(400).json(error);
    });
  },
  getMine: (req, res, next) => {
    console.log('Mine')
    console.log('req.decode', req.decoded)
    db.models.Thing.findAll(
      {
        where: {
          userId: req.decoded.sub
        },
        include: [ 'category', 'user', 'type']
      }
    ).then(things => {
      //console.log('things', things)
      res.status(200).json(things);
    }).catch(error => {
      console.log('error getMine', error)
      res.status(400).json(error);
    });
  },
  getAll: (req, res, next) => {
    console.log('inAll')
    db.models.Thing.findAll(
      {
        // where: {
        //   verified: true
        // },
        include: [ 'category', 'user', 'type']
      }
    ).then(things => {
      //console.log('things', things)
      res.status(200).json(things);
    }).catch(error => {
      res.status(400).json(things);
    });
  },
  find: (req, res, next) => {
    console.log('req', req.query)
    db.models.Thing.findOne({
      where: {id: req.params.id},
      include: [ 'category', 'user', 'type']
    }).then(thing => {
      return findThingFit.findLost(thing.dataValues, req.decoded)
    }).then((thingsFound)=> {
      res.status(200).json(thingsFound)
    }).catch(error => {
      console.log('error', error)
      res.status(400).json(error);
    });
    
  }
};
