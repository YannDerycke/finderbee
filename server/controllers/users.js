const jwt = require("jsonwebtoken");
const bcrypt = require('../utils/crypt');
const jwtService = require("../utils/jswToken");
// const User = require("../models/user");
const { JWT_SECRET } = require("../configuration");
const db = require("../db");
const sgMail = require("@sendgrid/mail");
const formidable = require("formidable");
const sendGridVersion = require ('../utils/sendGridVersions');

signToken = user => {
  return JWT.sign(
    {
      iss: "CodeWorkr",
      sub: user.id,
      iat: new Date().getTime(), // current time
      exp: new Date().setDate(new Date().getDate() + 1) // current time + 1 day ahead
    },
    JWT_SECRET
  );
};

module.exports = {
  pushNotification: (req, res, next) => {
    console.log("req dec", req.decoded);
    console.log("req", req.body.pushnotification);
    db.models.User.update(req.body,
      {
        where: {
          id: req.decoded.sub
        },
        returning: true, // needed for affectedRows to be populated
        plain: false // makes sure that the returned instances are just plain objects
      }
      ).then(user => {
        res.status(201).json(user);
      }).catch(error => {
        console.log('error', error)
        res.status(422).json(error);
      })
    // res.status(200).json("{results:'bjbjb'}");
  },
  signUp: (req, res, next) => {
    db.models.User.findOne(
      {
        where: {
          email: req.body.email
        }
      }
    ).then(user => {
      if (!user) {
        bcrypt.hash(req.body.password).then(function(hash) {
          req.body.password = hash;
          req.body.profile = "user";
          db.models.User.create(req.body
          ).then(user => {
            console.log('user1', user)
            res.status(201).send({message: "accountCreated"});
            const token = jwtService.signToken(user);
            db.models.VerificationToken.create(
              {
                userId: user.id,
                token: hash
              }
              ).then(verifyToken => {
                console.log('user2', user)
                sgMail.setApiKey(process.env.SENDGRID_API_KEY);
                const msg = {
                  to: "yann.derycke@gmail.com",
                  from: "info@finderbee.com",
                  templateId: true ? "d-72270a7fea6d49c58d6efa07d15b5816":"d-552a8b0afa7b422f92cd7efc1bef6a3e", //FINISH LOCAL
                  dynamic_template_data: {
                    subject: "Reset Password",
                    url:  `http://localhost:3000/users/verify?token=${verifyToken.token}&email=${user.email}`,
                    name: `${user.name} ${user.surname}`,
                    urlname: "Reset Password",
                    Sender_adress: "17, rue du Canada 1190 Forest"
                  }
                };
                sgMail.send(msg);
            })
          }).catch(error => {
            console.log('error', error)
            res.status(422).send({message: error});
          })
        });
      } else {
        res.status(409).send({message:"emailExists"});
      }
  
    });
  },
  signIn2: (req, res, next) => {
    console.log('reqqqq', req),
    console.log('ressssss', res),
    res.status(200).send({
      user: user,
      ok: true,
      message: "successSignIn",
      token
    });
  },
  signIn: (req, res, next) => {

    db.models.User.findOne(
      {
        where: {
          email: req.body.email
        }
      }
    ).then(user => {
      if (user) {
        bcrypt.compare( req.body.password, user.password).then(function(check) {
          if (check) {
            const token = jwtService.signToken(user);
            res.status(200).send({
              user: user,
              ok: true,
              message: "successSignIn",
              token
            });
          } else {
            res.status(401).send({
              ok: false,
              message: "errorSignIn"
            });
          }
        });
      } else {
        res.status(409).send({message:"errorSignIn"});
      }
  
    });
  },
  verify: (req, res, next) => {
    // console.log('email', req.query.email)
    // console.log('verificationToken', req.query.token)
     db.models.User.findOne({
      where: { email: req.query.email }
    })
      .then(user => {
        //console.log('user', user)
        if (user.verified) {
          return res.status(202).json(`Email Already Verified`);
        } else {
          db.models.VerificationToken.findOne({
            where: { token: req.query.token }
          })
            .then((foundToken) => {
              if(foundToken){
                user
                  .update({ verified: true })
                  .then(updatedUser => {
                    return res.status(403).json(`User with ${user.email} has been verified`);
                  })
                  .catch(reason => {
                    return res.status(403).json(`Verification failed`);
                  });
              } else {
                return res.status(404).json(`Token expired` );
              }
            })
            .catch(reason => {
              return res.status(404).json(`Token expired2`);
            });
        }
      })
      .catch(reason => {
        return res.status(404).json(`Email not found`);
      });
  },
  forgotPassword: (req, res, next) => {
    db.models.User.findOne(
      {
        where: {
          email: req.body.email
        }
      }
    ).then(user => {
      if (user) {
        // const secret = user.password + '-' + user.createdAt.getTime();
        // console.log('secret', secret)
        const token = jwtService.signToken(user);
        // using SendGrid's v3 Node.js Library
        // https://github.com/sendgrid/sendgrid-nodejs

        //SENDGRID !!!!!!!
        console.log('user', user)

        sgMail.setApiKey(process.env.SENDGRID_API_KEY);
        const msg = {
          to: user.email,
          from: "info@finderbee.com",
          templateId: sendGridVersion.resetPassword[user.language],
          //templateId: true ? "d-75553597e7d54c298dbe35d20af1f6d9":"d-552a8b0afa7b422f92cd7efc1bef6a3e", //FINISH LOCAL
          dynamic_template_data: {
            subject: "Reset Password",
            url:  `http://localhost:3000/users/resetPasswordForm/${token}/${user.language}`,
            name: `${user.name} ${user.surname}`,
            urlname: "Reset Password",
            Sender_adress: "17, rue du Canada 1190 Forest"
          }
        };
        sgMail.send(msg);
        res.status(200).send({
          user: user,
          ok: true,
          message: "Login successful"
        });
      } else {
        res.status(409).send("Email incorrect !");
      }
  
    });
  },
  resetPasswordForm: (req, res, next) => {
    console.log('resetPasswordForm', req.params.token)
    console.log('resetPasswordForm', req.params.language)
    const checkToken = jwtService.checkToken(req.params.token)
    const message = {
      fr: {placeholder: "Changer votre mot de passe", submit:"Valider"},
      en:  {placeholder:"Change your password", submit:"Validate"},
      es:  {placeholder:"Cambia tu contraseña", submit:"Validar"}
    }
    if(checkToken) {
      res.send('<form action="/users/resetPassword/' + req.params.language + '" method="POST">' +
        '<input type="hidden" name="id" value="' + checkToken.sub + '" />' +
        '<input type="hidden" name="token" value="' + req.params.token + '" />' +
        '<input type="password" name="password" value="" placeholder="' + message[req.params.language].placeholder+ '" />' +
        '<input type="submit" value="' + message[req.params.language].submit + '" />' +
    '</form>');
    } else {
      res.sendStatus(403);
    }
  },
  resetPassword: (req, res, next) => {
    var form = new formidable.IncomingForm();
    let userInfos;
    form.parse(req, function(err, fields, files) {
      if (err) {
        console.error("Error", err);
        throw err;
      }
      userInfos = fields;
    });
    try {
      form.on("end", function(fields, files) {
        console.log("userInfos", userInfos);
        const password = userInfos.password;
        const checkToken = jwtService.checkToken(userInfos.token)
        if(checkToken && password) {
          bcrypt.hash(password).then(function(hash) {
            userInfos.password = hash;
            db.models.User.update( userInfos, {
              where: {id: checkToken.sub},
              returning: true, // needed for affectedRows to be populated
              plain: false // makes sure that the returned instances are just plain objects
            }
            ).then(user => {
              res.status(201).send('Password changed');
            }).catch(error => {
              console.log('error', error)
              res.status(422).json(error);
            })
          });
        } else {
          res.sendStatus(403);
        }
      })
    }
    catch {

    }
  },
  getMe: (req, res, next) => {
    console.log('rea', req.decoded)
    db.models.User.findOne(
      {
        where: {
          id: req.decoded.sub
        }
      }
    ).then(users => {
      res.status(200).json(users);
    }).catch(error => {
      res.status(400).json(error);
    });
  },
  getAll: (req, res, next) => {
    db.models.User.findAll(
      // {
      //   where: {
      //     id: 1
      //   }
      // }
      {include: [ 'thing']}
    ).then(users => {
      res.status(200).json(users);
    }).catch(error => {
      res.status(400).json(error);
    });
  },
  update: (req, res, next) => {
    console.log('req.decoded', req.decoded.sub)
    delete req.body.password;
    db.models.User.update(
      req.body, {
        where: {id: req.decoded.sub},
        returning: true, // needed for affectedRows to be populated
        plain: false // makes sure that the returned instances are just plain objects
      }
      ).then(([ rowsUpdate, [updatedUser]]) => {
        console.log('updatedUser', updatedUser)
        console.log('rowsUpdate', rowsUpdate)
        if(rowsUpdate) {
          res.status(200).json(updatedUser);
        } else {
          res.status(400).json({message:"no entry"});
        }
        
      }).catch(error => {
        console.log('error', error)
        res.status(422).json(error);
      })
    
  },
  password: (req, res, next) => {
    console.log('req.decoded', req.decoded.sub)
    console.log('req.password', req.body.oldPassword)
    // delete req.body.password;
    const userInfos = {password: req.body.oldPassword};
    db.models.User.findOne(
      {
        where: {
          id: req.decoded.sub
        }
      }
    ).then(user => {
      console.log('user', user)
      bcrypt.compare( req.body.oldPassword, user.password).then(function(check) {
        console.log('check', check)
        if (check) {
          bcrypt.hash(req.body.password).then(function(hash) {
            console.log('hash', hash)
            db.models.User.update( {password:hash}, {
              where: {id: req.decoded.sub},
              returning: true, // needed for affectedRows to be populated
              plain: false // makes sure that the returned instances are just plain objects
            }
            ).then(user => {
              res.status(201).send('Password changed');
            }).catch(error => {
              console.log('error', error)
              res.status(422).json(error);
            })
          })
          
        } else {
          res.status(401).send({
            ok: false,
            message: "errorSignIn"
          });
        }
      })
    })
    .catch(error=> {
      console.log('error', error)
    })
  },

  googleOAuth: async (req, res, next) => {
    // Generate token
    console.log("req.user", req.user);
    const token = signToken(req.user);
    res.status(200).json({ token });
  },

  linkGoogle: async (req, res, next) => {
    res.json({
      success: true,
      methods: req.user.methods,
      message: "Successfully linked account with Google"
    });
  },

  unlinkGoogle: async (req, res, next) => {
    // Delete Google sub-object
    if (req.user.google) {
      req.user.google = undefined;
    }
    // Remove 'google' from methods array
    const googleStrPos = req.user.methods.indexOf("google");
    if (googleStrPos >= 0) {
      req.user.methods.splice(googleStrPos, 1);
    }
    await req.user.save();

    // Return something?
    res.json({
      success: true,
      methods: req.user.methods,
      message: "Successfully unlinked account from Google"
    });
  },

  facebookOAuth: async (req, res, next) => {
    // Generate token
    fetch(
      `https://graph.facebook.com/me?access_token=${token}&fields=id,birthday,name,email`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      }
    );
    const token = signToken(req.user);
    res.status(200).json({ token });
  },

  linkFacebook: async (req, res, next) => {
    res.json({
      success: true,
      methods: req.user.methods,
      message: "Successfully linked account with Facebook"
    });
  }, 

  unlinkFacebook: async (req, res, next) => {
    // Delete Facebook sub-object
    if (req.user.facebook) {
      req.user.facebook = undefined;
    }
    // Remove 'facebook' from methods array
    const facebookStrPos = req.user.methods.indexOf("facebook");
    if (facebookStrPos >= 0) {
      req.user.methods.splice(facebookStrPos, 1);
    }
    await req.user.save();

    // Return something?
    res.json({
      success: true,
      methods: req.user.methods,
      message: "Successfully unlinked account from Facebook"
    });
  },

  dashboard: async (req, res, next) => {
    console.log("I managed to get here!");
    res.json({
      secret: "resource",
      methods: req.user.methods
    });
  }
};
