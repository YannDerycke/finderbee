const Sequelize = require("sequelize");
const db = require("../db");
const sgMail = require("@sendgrid/mail");
const pushNotification =  require('./pushNotification');
const { Expo } = require("expo-server-sdk");
const sendGridVersion = require ('../utils/sendGridVersions');
module.exports = {
    find: (thingToCheck, user) => {
        let queryGeo = "";
        let typeId = "";
        if (thingToCheck.typeId === 2 ) { /// FOUND === 2 !!! find SOLUTION AVOID ID
            console.log('found')
            queryGeo = Sequelize.fn('ST_DWithin',
                Sequelize.col('geo'),
                Sequelize.fn('ST_MakePoint', parseFloat(thingToCheck.geo.coordinates[0]), parseFloat(thingToCheck.geo.coordinates[1])),
                Sequelize.col('radius')
            )
            typeId = 3 // LOST
        }  else { 
            console.log('lost')
            queryGeo = Sequelize.fn('ST_DWithin',
                Sequelize.col('geo'),
                Sequelize.fn('ST_MakePoint', parseFloat(thingToCheck.geo.coordinates[0]), parseFloat(thingToCheck.geo.coordinates[1])),
                parseFloat(thingToCheck.radius)
            ) 
            typeId = 2 //FOUND
        }

        let where = {
            $and:  Sequelize.where(queryGeo, true),
            verified: true,
            typeId,
            userId: { [Sequelize.Op.not]: user.sub},
            categoryId: thingToCheck.categoryId
        }
        console.log('where', where)

        //console.log('data', data)

        db.models.Thing.findAll({
            where:where,
            include: [ 'category', 'user', 'type']}     
        )
        .then((things) =>{
           console.log('things found', things)
          
           //console.log('thing. thingToCheck', thingToCheck)
           let messages = []
           if (thingToCheck.typeId === 2) { /// FOUND
            console.log('found added')
                things.forEach(thing => {
                    //console.log('thing. user', thing.user)
                    let isExpoToken = Expo.isExpoPushToken(thing.user.pushnotification);
                    if (!isExpoToken) {
                        console.error(`Push token ${thing.user.pushnotification} is not a valid Expo push token`);
                    }
                    if(thing.user.pushnotification && isExpoToken) {//TO DO check if message for same user already exist...
                        messages.push({
                            to:thing.user.pushnotification,
                            sound: "default",
                            title: "Objet !!",
                            body: "Objet trouvé qui pourrait vous intéresser",
                            data: {"screen": "DetailsScreen", "id": thingToCheck.id }
                        })
                    }
                    let notification = {
                        userId: thing.user.id,
                        thingId: thingToCheck.id,
                        type: "FOUND",
                        status: "VISIBLE"
                    }

            //         "INSERT INTO product_alert
			// (product_id, product_url, email)
			// SELECT :product_id, :product_url::text, :email::text, 
			// WHERE
			// NOT EXISTS (
			// 	SELECT product_id FROM product_alert WHERE email = :email AND product_id = :product_id
			// 	);
                    db.models.Notification.findOrCreate(
                        {where: {
                            userId: notification.userId,
                            thingId: notification.thingId
                          },
                          defaults: { // set the default properties if it doesn't exist
                          type: "FOUND",
                          status: "VISIBLE"
                          }    
                        }
                        ).then(notif => {
                            //console.log('notif', notif)
                        }).catch(error => {
                          console.log('error', error)
                        })
                    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
                    
                    const msg = {
                    to: thing.user.email,
                    from: "info@finderbee.com",
                    templateId: sendGridVersion.foundAdded[thing.user.language],
                    //templateId: true ? "d-948e60c4a5d94d3e83a3e5f4774f319f":"", //FINISH LOCAL Thing Found Added FR
                    dynamic_template_data: {
                        subject: "FOUND",
                        thing: thingToCheck,
                        name: `${thing.user.name} ${thing.user.surname}`,
                        Sender_adress: "17, rue du Canada 1190 Forest"
                    }
                    };
                    sgMail.send(msg);
                });
                pushNotification.sendNotification(messages)
           } else { //LOST

            db.models.User.findOne(
                {
                  where: {
                    id: user.sub
                  }
                }
              ).then(user => {
                if (user) {
                    //console.log('lost added user', user)
                    let isExpoToken = Expo.isExpoPushToken(user.pushnotification);
                    if (!isExpoToken) {
                        console.error(`Push token ${user.pushnotification} is not a valid Expo push token`);
                    }
                    if(user.pushnotification && isExpoToken) {
                        messages.push({
                            to:user.pushnotification,
                            sound: "default",
                            title: things.length + "Objets !!",
                            body: "Objets trouvés qui pourraientt vous intéresser",
                            data: {"screen": "NotificationScreen" }
                        })
                    }

                    things.forEach(thing => {
                        let notification = {
                            userId: user.id,
                            thingId: thing.id,
                            type: "FOUND",
                            status: "VISIBLE"
                        }
                        //console.log('notif db', notification)
                        db.models.Notification.findOrCreate(
                            {
                                where: {
                                    userId: notification.userId,
                                    thingId: notification.thingId
                                },
                                defaults: { // set the default properties if it doesn't exist
                                type: "FOUND",
                                status: "VISIBLE"
                                }    
                            }
                            ).then(notif => {
                                //console.log('notif', notif)
                            }).catch(error => {
                              console.log('error', error)
                            })
                    })
                    pushNotification.sendNotification(messages)
                    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
                    const msg = {
                    to: user.email,
                    from: "info@finderbee.com",
                    templateId: sendGridVersion.lostAdded[user.language],
                    //templateId: true ? "d-cd41cc1546fc4007bfb4b5e916c06b0d":"", //FINISH LOCAL LOST ADDED
                    dynamic_template_data: {
                        subject: "FOUND",
                        things,
                        name: `${user.name} ${user.surname}`,

                        Sender_adress: "17, rue du Canada 1190 Forest"
                    }
                    };
                    sgMail.send(msg);
                  
                } else {
                  res.status(409).send({message:"errorSignIn"});
                }
            
              });
            
           }
           
           
        //res.send(things)
        
        } )
        .catch()
        }
}