const router = require("express-promise-router")();
const jwtService = require("../utils/jswToken");
const CategoriesController = require("../controllers/categories");

router.route("/").get(jwtService.ensureToken, CategoriesController.getAll);
router.route("/").post(jwtService.ensureToken, CategoriesController.save);

module.exports = router;
