const router = require("express-promise-router")();
const jwtService = require("../utils/jswToken");
const NotificationsController = require("../controllers/notifications");

router.route("/mine").get(jwtService.ensureToken, NotificationsController.getMine);
router.route("/:id").delete(jwtService.ensureToken, NotificationsController.erase);


module.exports = router;