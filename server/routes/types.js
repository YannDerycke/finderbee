const router = require("express-promise-router")();
const jwtService = require("../utils/jswToken");
const TypesController = require("../controllers/types");

router.route("/").get(jwtService.ensureToken, TypesController.getAll);
router.route("/").post(jwtService.ensureToken, TypesController.save);

module.exports = router;
