const router = require("express-promise-router")();
const PictureController = require("../controllers/pictures");
const ThingsController = require("../controllers/things");
const db = require("../db");
const cloudinary = require("cloudinary");
const formidable = require("formidable");
cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
});

// router.post("/upload/:userId", function(req, res) {
//   console.log('upload');
//   var form = new formidable.IncomingForm();
//   let currentFields;
//   form.parse(req, function(error, fields, files) {
//     if (error) {
//       res.status(422).json(error);
//     }
//     currentFields = fields;
//   })
//   try {
//     form.on("end", function(fields, files) {
//       var temp_path = this.openedFiles[0].path;
//       var file_name = this.openedFiles[0].name;
//       // let x = cloudinary.url("download.png", {
//       //   width: 100,
//       //   height: 150,
//       //   crop: "fill"
//       // });
//       // console.log("crop", x);
//       cloudinary.uploader.upload(
//         temp_path,
//         function(result) {
//           const payload = {
//             userId : req.params.userId,
//             categoryId: currentFields.category,
//             description : currentFields.description,
//             geo : { type: 'Point', coordinates: [4.3333336, 50.849]},
//             picture: result.url
//           }
//           console.log("payload", payload);
//           if (!result.error) {
//             db.models.Thing.create(payload,
//               ).then(user => {
//                 res.status(201).json(user);
//               }).catch(error => {
//                 console.log('error', error)
//                 res.status(422).json(error);
//               })
//             return;
//           } else {
//             res.status(400).json(result.error);
//           }
//         },
//         {
//           public_id: file_name,
//           folder: "finderBee/jean",
//           chunk_size: 6000000,
//           eager: [
//             { width: 50, height: 50, crop: "pad", audio_codec: "none" },
//             {
//               width: 160,
//               height: 100,
//               crop: "crop",
//               gravity: "south",
//               audio_codec: "none"
//             }
//           ],
//           eager_async: true,
//           eager_notification_url: "https://mysite.example.com/notify_endpoint"
//         }
//       );
//     });
//   } catch (error) {
//     res.send("error");
//   }
// });

module.exports = router;

// {
//     "error": {
//         "message": "Invalid api_key 55135376876259",
//             "http_code": 401
//     }
// }
