const router = require("express-promise-router")();
const expo = require("../controllers/pushNotification");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const jwtService = require("../utils/jswToken");
const profilecheck = require("../utils/checkProfile");
const username = "yann";
const password = "123";

const ThingsController = require("../controllers/things");

//router.route("/:userId").get(jwtService.ensureToken, ThingsController.get);

router.route("/").get(jwtService.ensureToken, profilecheck('admin'), ThingsController.getAll);
router.route("/mine").get(jwtService.ensureToken, ThingsController.getMine);
router.route("/find/:id").get(jwtService.ensureToken, ThingsController.find);
router.route("/:id").get(jwtService.ensureToken, ThingsController.get);
router.route("/").post(jwtService.ensureToken, ThingsController.save);
router.route("/:id").put(jwtService.ensureToken, ThingsController.update);
router.route("/:id").delete(jwtService.ensureToken, ThingsController.erase);
module.exports = router;
