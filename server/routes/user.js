
const router = require("express-promise-router")();
const expo = require("../controllers/pushNotification");
console.log("router", router);
const passport = require("passport");
const jwtService = require("../utils/jswToken");
const profilecheck = require("../utils/checkProfile");
const db = require("../db");
require('../passport')(passport)

const UsersController = require("../controllers/users");
const ThingsController = require("../controllers/things");

router
  .route("/oauth/facebook")
  .post(
    passport.authenticate("facebookToken", { session: false }),
    UsersController.facebookOAuth
  );


router.route("/pushnotification").post(jwtService.ensureToken, UsersController.pushNotification);
router.route("/me").get(jwtService.ensureToken, UsersController.getMe);
router.route("/password").put(jwtService.ensureToken, UsersController.password);
router.route("/").get(jwtService.ensureToken, profilecheck('admin'), UsersController.getAll);
router.route("/signUp").post(UsersController.signUp);
router.route("/:userId/things").get(jwtService.ensureToken, ThingsController.get);
router.route("/").put(jwtService.ensureToken, UsersController.update);
// router.route('/signIn').post(
//   passport.authenticate('local', { 
                                   
//                                    failureFlash: true }, UsersController.signIn)
// );
router.route("/signIn").post(UsersController.signIn);

router.route("/verify").get(UsersController.verify);

router.route("/forgotPassword").post(UsersController.forgotPassword);
router.route("/resetPasswordForm/:token/:language").get(UsersController.resetPasswordForm);
router.route("/resetPassword/:language").post(UsersController.resetPassword);

router.route("/loginFacebook").post(function(req, res) {
  console.log("req.body", req.body.userFacebook);
  const userFacebook = req.body.userFacebook
  ///check in DB the email
  db.models.User.findOrCreate(
    {where: {
        email: userFacebook.email,
      },
      defaults: { // set the default properties if it doesn't exist
      name: userFacebook.first_name,
      surname: userFacebook.last_name,
      profile: "user",
      verified: false,
      pushnotification: userFacebook.pushNotification
      }    
    }
    ).then(([user, created]) => {
        console.log('user findorcreate', user)
        
        const token = jwtService.signToken(user);
        res.status(200).send({
          user: user,
          ok: true,
          message: "successSignIn",
          token
        });
    }).catch(error => {
      console.log('error', error)
    })
  // if (req.body.userFacebook.email == "yann.derycke@gmail.com") {
  //   console.log('in')
  //   const user = { id: 1 };
  //   const token = jwtService.signToken(user);
  //   res.status(200).json({
  //     ok: true,
  //     message: "Login successful",
  //     token
  //   });

  // } else {
  //   console.log('out')
  //   res.send({
  //     ok: false,
  //     message: "Username or password incorrect"
  //   });
  // }
});

module.exports = router;
